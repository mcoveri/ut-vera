UT-VERA Installation Scripts
============================

Contact
-------

William Gurecky

william.gurecky@utexas.edu

Erich Schneider

eschneider@mail.utexas.edu


Introduction
------------

The Virtual Environment for Reactor Applications (VERA) is a central component of the Consortium for Advanced Simulation of LWRs’ (CASL) technical portfolio.
The Virtual Environment for Reactor Applications – Core Siumulator (VERA-CS) is a muti-scale, multi-physics package being developed by Oak Ridge National Laboratory in collaboration with multiple universities and national laboratories. It aims to create a holistic depiction of a nuclear reactor core in order to answer design and performance questions that had been out of reach of single-physics or asynchronously-coupled multiphysics codes.

The VERA-CS code suite is comprised of the following packages:

-	MPACT: method of characteristic transport code. Limited source availability / Proprietary
-	Exnihilo:  ORNL transport software bundle
	- Insilco: Sn transport code
	- Shift: Monte-carlo transport code
	- ORNL, unknown license.  Access to nuclear data requires SCALE license through RSICC.
-	COBRA-TF:  Subchannel thermal hydraulics code. Source available through RSICC
-	MAMBA:  CRUD/Fuel performance code. Limited source availability / Proprietary
-	Dakota:  Uncertainty quantification and data analysis. LGPL

Coupling of the above codes is primarily achieved through:

-	DTK: data transfer kit  ( https://github.com/CNERG/DataTransferKit ). BSD
-	LIME: c++ framework for multiphysics coupling  ( http://sourceforge.net/projects/lime1/). BSD
-	Tribits:  Build, testing, and integration suite.  Uses CMAKE/Ctest ( https://code.google.com/p/tribits/ ). BSD
-	Perl/python scripts (VERA ASCII input parsers and XML generators). ORNL Proprietary

Visit www.casl.gov for detailed information.  All of the above packages are hosted at ORNL on casl-dev.ornl.gov (casl-dev).

VERA-CS Build Dependencies:

-	GCC 4.6.1
-	Cmake 2.8.5+
-	OpenMPI 1.4.3
-	Python 2.7
-	Lapack 3.3.1
-	Boost 1.49.0
-	Zlib
-	Moab
-	Hypre 2.8.0 
-	HDF5 1.8.7
-	PETSc 3.3-p4
-	Silo

The installation instructions have been distilled into several Ansible playbooks.  The playbooks are available in the UT-VERA repository.  Ansible is ideal for managing / updating VERA-CS on a variety of machines simultaneously.  ( www.ansible.com )

File Listing
------------

/doc          contains MOU information and Ansible script documentation

/playbooks    contains the Ansible playbooks

/modules      shell scripts and environment module scripts


License
-------
Copyright (c) 2014 William Gurecky.
All rights reserved.

Redistribution and use in source and binary forms are permitted
provided that the above copyright notice and this paragraph are
duplicated in all such forms and that any documentation,
advertising materials, and other materials related to such
distribution and use acknowledge that the software was developed
by William Gurecky in support of the Consortium for the Advanced Simulation
of Light Water Reactors (CASL). 
The name of CASL may not be used to endorse or promote products derived 
from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED AS IS AND WITHOUT ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.