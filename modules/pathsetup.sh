#!/bin/bash

# Set dev env paths
VERA_DEV_ENV_BASE=/tools/vera
VERA_DEV_ENV_COMPILER_BASE=/tools/vera/gcc-4.6.1
VERA_GCC_BASE_DIR=/tools/vera/gcc-4.6.1/toolset/gcc-4.6.1
VERA_MPI_BASE_DIR=/tools/vera/gcc-4.6.1/toolset/openmpi-1.4.3
VERA_BoostLib_LIBRARY_DIRS=/tools/vera/gcc-4.6.1/tpls/opt/common/boost-1.49.0/lib
VERA_MOAB_LIBRARY_DIRS=/tools/vera/gcc-4.6.1/tpls/opt/common/moab-4.5.0/lib
VERA_HDF5_LIBRARY_DIRS=/tools/vera/gcc-4.6.1/tpls/opt/vera_cs/hdf5-1.8.7/lib
TPL_INSTALL_DIR=/tools/vera/gcc-4.6.1/tpls/opt

# Set the install base dir path
# Set paths to shared libs for compiler and MPI
export LD_LIBRARY_PATH=$VERA_MPI_BASE_DIR/lib:$VERA_GCC_BASE_DIR/lib64:$LD_LIBRARY_PATH

# TPL lib dirs (which use shared libs)
export LD_LIBRARY_PATH=$VERA_HDF5_LIBRARY_DIRS:$VERA_MOAB_LIBRARY_DIRS:$VERA_BoostLib_LIBRARY_DIRS:$LD_LIBRARY_PATH
